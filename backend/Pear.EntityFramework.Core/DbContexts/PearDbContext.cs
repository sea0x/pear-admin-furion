﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace Pear.EntityFramework.Core
{
    [AppDbContext("Furion", DbProvider.Sqlite)]
    public class PearDbContext : AppDbContext<PearDbContext>
    {
        public PearDbContext(DbContextOptions<PearDbContext> options) : base(options)
        {
        }
    }
}